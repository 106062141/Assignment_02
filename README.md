# Software Studio 2019 Spring Assignment 2
## Notice
* Replace all [xxxx] to your answer

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Complete game process|15%|N|
|Basic rules|20%|N|
|Jucify mechanisms|15%|N|
|Animations|10%|N|
|Particle Systems|10%|N|
|UI|5%|N|
|Sound effects|5%|N|
|Leaderboard|5%|N|

## Website Detail Description

# Basic Components Description : 
1. Jucify mechanisms : 有兩關，第一關過了才會有第二關。第一關有兩個絕招可以用，x跟z鍵，x是護頓，可以無敵三秒，z是敵人全滅，偷雞摸狗的人才用的。
                        第二關沒有絕招可以用(所以比較難)，
2. Animations : 第一關的敵人有動畫，第一關的X無敵型態有動畫，第二關的主角腳色有動畫。
3. Particle Systems : 第二關的敵人子彈是用emitter做出來的，每一顆子彈都是particle。
4. Sound effects : 有背景音樂，跟空白鍵射擊的音效。用u/d可以升/降音量。p可以暫停遊戲。
5. Leaderboard : 死掉之後輸入名字會進入。Leaderboard中按enter可以回到menu。

# Bonus Functions Description : 
1. [xxxx] : [xxxx]
2. [xxxx] : [xxxx]
3. [xxxx] : [xxxx]
