var game = new Phaser.Game(800, 600, Phaser.AUTO, 'canvas');
game.state.add('menu',menuState)
game.state.add('template',tempState);
game.state.add('gameover',gameover);
game.state.add('play',playState);
game.state.start('menu');
