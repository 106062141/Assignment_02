var btnstart;
var stateText;
var menuState={
    preload: function(){
        game.load.spritesheet('btnstart','asset/button-start-spritesheet.png',201,215/3);
    },
    create: function(){
        game.stage.backgroundColor = '#3498db';
        //btnstart = game.add.sprite(game.width/2, game.height/2, 'btnstart');
        //btnstart.animations.add('start', [0,1,2], 10, true);
        btnstart = game.add.button(game.world.centerX,game.world.centerY, 'btnstart', this.actionOnClick, this, 1, 0, 2);//over out down up

        stateText = game.add.text(200,200,' fire: space \n pause: p \n volume up,down: U,D \n', { font: '40px Arial', fill: '#fff' });
        stateText.anchor.setTo(0.5, 0.5);
    
    },
    update: function(){

    },
    actionOnClick: function() {

        game.state.start('template');

    
    }
}