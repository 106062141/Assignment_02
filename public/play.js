//var game = new Phaser.Game(600, 500, Phaser.AUTO, 'canvas',{ preload: preload, create: create, update: update});
    var plane;
    var bullets;
    var spacebar;
    var cursor;
    var nextFire=0;
    var fireRate=100;
    var enemygroup;
    var enemy;
    var emitter = [];
    var i;
    var score;
    var lives;
    var key_Z;
    var lives;
    var explosions;
    var stateText;
    var pause;
    var name;
    //var explosion;
    var playState = {
    preload: function(){
        game.load.spritesheet('plane','asset/humstar.png',32,32);//每個獨立個體是幾pix//用小畫家框起來看
        game.load.spritesheet('enemy', 'asset/invader32x32x4.png', 32, 32);
        game.load.image('enemy_bullet','asset/bullet44.png')
        game.load.image('background','asset/background.jpg');
        game.load.image('bullet','asset/bullet1.png');
        game.load.image('ship','asset/player.png');
        game.load.spritesheet('kaboom', 'asset/explode.png', 128, 128);
        game.load.audio('gunsound', 'asset/gun sound.wav');
        game.load.audio('boden', 'asset/bodenstaendig_2000_in_rock_4bit.mp3');
        game.load.image('pixel','asset/pixel.png');
    },
    create: function(){
        game.stage.backgroundColor = '#3498db';
        //game.add.image(0, 0, 'background'); 
        this.background = game.add.tileSprite(0, 0, 800, 600, 'background');//會自動延伸圖片
        game.world.setBounds(0, 0, 800, 600);
        game.physics.startSystem(Phaser.Physics.ARCADE);
        
        
        cursor = game.input.keyboard.createCursorKeys();
        spacebar = game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
        
        plane = game.add.sprite(game.width/2, game.height, 'plane');
        plane.animations.add('fly', [0,1,2,3,4,5], 10, true);
        plane.animations.play('fly');
        plane.anchor.set(0.5);//中心
        //game.camera.follow(plane, Phaser.Camera.FOLLOW_LOCKON, 0.1, 0.1);
        //這兩行不知道為啥都沒用 要邊界停要用第三行
        //plane.physicsBodyType = Phaser.Physics.ARCADE;
        //plane.enableBody = true;
        game.physics.enable(plane,Phaser.Physics.ARCADE);
        plane.body.collideWorldBounds = true;
        
        bullets = game.add.group();
        bullets.enableBody = true; 
        bullets.createMultiple(50, 'bullet');
        bullets.setAll('checkWorldBounds', true);
        bullets.setAll('outOfBoundsKill', true);

        this.create_enemy();

        scoreString = 'Score : ';
        scoreText = game.add.text(10, 10, scoreString + score, { font: '34px Arial', fill: '#fff' });

        
        //複製過來的
        lives = game.add.group();
        game.add.text(game.world.width - 100, 10, 'Lives : ', { font: '34px Arial', fill: '#fff' });

        for (var j = 0; j < 3; j++) 
        {
            var ship = lives.create(game.world.width - 100 + (30 * j), 60, 'ship');
            ship.anchor.setTo(0.5, 0.5);
            ship.angle = 90;
            ship.alpha = 0.4;
        }
        //explosions
        explosions = game.add.group();
        explosions.createMultiple(30, 'kaboom');

        stateText = game.add.text(game.world.centerX,game.world.centerY,' ', { font: '84px Arial', fill: '#fff' });
        stateText.anchor.setTo(0.5, 0.5);
        stateText.visible = false;

        music = game.add.audio('boden');
        music.play();

        gunsound = game.add.audio('gunsound');
        pause = game.input.keyboard.addKey(Phaser.Keyboard.P);
    },
    update: function(){
        this.background.tilePosition.y += 2;
        if(plane.alive){
            this.movePlayer();
            enemygroup.forEachExists(this.enemy_move, this);
            if(enemygroup.countLiving()<1){
                music.destroy();
                stateText.text=" GAME OVER \n Click to leaderboard";
                stateText.visible = true;
                name = prompt("Please enter your name", "name");
                firebase.database().ref().push({
                    name:name,
                    score:score,
                }); 
                game.input.onTap.addOnce(function(){
                game.state.start('gameover');
            },this);
          
            }
            game.physics.arcade.overlap(bullets,enemygroup,function(bullet,enemy){
                bullet.kill();
                enemy.kill();
                enemy.children[0].destroy();
                //enemy.kill();
                
                score += 20;
                scoreText.text = scoreString + score;

                var explosiond = game.add.sprite(enemy.x, enemy.y, 'kaboom');
                explosiond.anchor.setTo(0.5);
                explosiond.animations.add('boom',[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15],0,false);
                explosiond.play('boom', 80, false, true);
            },null,this)
        }

        if(game.input.keyboard.justPressed(Phaser.Keyboard.U)) {
            if(music.volume<=1){
                music.volume += 0.1;
                gunsound.volume +=0.1;
            }
            if(music.volume>1) music.volume=1;
            if(gunsound.volume >1) gunsound.volume=1;
        }else if(game.input.keyboard.justPressed(Phaser.Keyboard.D)){
            if(music.volume>0){
                music.volume -= 0.1;
                gunsound.volume -=0.1;
            }
            if(music.volume<0) music.volume=0;
            if(gunsound.volume <0) gunsound.volume=0;
        }
        window.onkeydown = function() {
            if (game.input.keyboard.event.keyCode == 80){
                game.paused = !game.paused;
            }
        }
        

        
   
        
    },
    movePlayer: function() { 
        if (cursor.left.isDown) plane.x -=10;
        else if (cursor.right.isDown) plane.x +=10; 
   
        if (cursor.up.isDown) plane.y -=10; 
        else if(cursor.down.isDown)plane.y +=10;

        if(spacebar.isDown) {
            this.firebullet();
            gunsound.play();
        }
 
    },
    firebullet: function(){
        if (game.time.now > nextFire && bullets.countDead() > 0)
        {
            nextFire = game.time.now + fireRate;

            var bullet = bullets.getFirstDead();

            bullet.reset(plane.x - 8, plane.y - 50);
            bullet.body.velocity.y = -300;
           
        }

    },
    create_enemy: function(){
        enemygroup = game.add.group();
        enemygroup.enableBody = true; 
        for(i=0;i<10;i++){
            enemy = game.add.sprite(game.world.randomX,game.rnd.integerInRange(0, 200),'enemy');
            enemy.anchor.set(0.5,0.5);
            game.physics.enable(enemy,Phaser.Physics.ARCADE);//不加這行 body會變成null
            //enemy.body.velocity.y = 0;
            this.enemy_shoot();
            enemy.addChild(emitter);
            enemy.checkWorldBounds = true;
            enemy.animations.add('kaboom');
            enemy.events.onOutOfBounds.add(this.enemyOut, this);
            enemygroup.add(enemy);//這兩行等於下一行
           //enemygroup.create(game.world.randomX,game.world.randomY,'enemy');
        }
        //enemygroup.animations.add('kaboom');
    },
    enemy_shoot: function(){
        emitter = game.add.emitter(0, 0,5);
        
        emitter.makeParticles('enemy_bullet',0,5,true,false);
        emitter.setYSpeed(-300, 300);//和上面是一樣的意思
        emitter.setXSpeed(-300, 300);
        emitter.minRotation = 0;//噴出去的東西會不會旋轉
        emitter.maxRotation = 0;
        emitter.gravity = 0;
        
    },
    enemy_move: function(enemy){
        var speed = 100;
        enemy.body.velocity.y += game.time.physicsElapsed * speed;
        //enemy.body.collideWorldBounds = true;
        enemy.children[0].start(true, 2000, null,5);
        game.physics.arcade.overlap(plane,enemy.children[0],this.hitplayer,null,this);
        
        
        
    },
    enemyOut :function (enemy0){
        enemy0.reset(game.world.randomX,game.rnd.integerInRange(0, 200));
    },
    hitplayer: function(){
        live = lives.getFirstAlive();
        if (live) live.kill();
        
        /*explosion = explosions.getFirstExists(false);
        explosion.reset(plane.body.x, plane.body.y);
        explosion.play('kaboom', 30, false, true);
        */
       
        if (lives.countLiving() < 1)
        {
            plane.kill();
            music.destroy();
            var emit = game.add.emitter(plane.x, plane.y, 15);
            emit.makeParticles('pixel');
            emit.setYSpeed(-150, 150);
            emit.setXSpeed(-150, 150);
            emit.setScale(2, 0, 2, 0, 800);
            emit.gravity = 500;
            emit.start(true,800,null,15);

            stateText.text=" GAME OVER \n Click to leaderboard";
            stateText.visible = true;
            name = prompt("Please enter your name", "name");
            firebase.database().ref().push({
                name:name,
                score:score,
            }); 
            game.input.onTap.addOnce(function(){
                game.state.start('gameover');
            },this);
          
				
				
            

            
        }
    }
}



